# https://github.com/mrtaddy/omg-hubot/commit/4ee1e402c1503343e30fcfad5104573c30d11e34
# Description
#   kenchankunsan
#
# Commands:
#   hubot kenchankunsan me - generates kenchankunsan
#
# Author:
#   TAKAHASHI Kazunari[takahashi@1syo.net]

module.exports = (robot) ->
  robot.hear /kenchankunsan/, (msg) ->
    msg.http("http://kenchankunsan.herokuapp.com/.txt")
      .get() (err, res, body) ->
        unless err
          msg.send body.replace(/\n/g, "")
